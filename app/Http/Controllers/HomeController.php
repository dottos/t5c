<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {	
        return view('home');
    }
	
	public function create(Request $request){
		$validator = Validator::make($request->all(),[
			'reference' => 'required|unique:invoices',
			'amount' => 'required|numeric',
			'description' => 'required'
		]);
		
		if($validator->fails()){
			return redirect('/create')->withErrors($validator)->withInput();
		}
		
		$invoice = new \App\Invoice;
		
		$invoice->reference = $request->reference;
		$invoice->amount = $request->amount;
		$invoice->description = $request->description;
		
		$invoice->save();
		
		$log = \App\Logs::create([
			'added_on' => date('Y-m-d H:i:s', time()),
			'user_id' => Auth::id(),
			'description' => 'has been added an invoice'
		]);
		
		return redirect('/home');
	}
}
