<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logs extends Model
{
	protected $table = 'user_logs';
	protected $fillable = ['added_on','user_id','description'];
	
    public $timestamps = false;
	public $autoincrement = false;
	
}
