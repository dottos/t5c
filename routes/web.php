<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/logs', function(){	
	if(!Auth::user()->is_admin){
		$log = \App\Logs::create([
			'added_on' => date('Y-m-d H:i:s', time()),
			'user_id' => Auth::id(),
			'description' => 'tried to view logs'
		]);
		
		return redirect('/home');
	}
	
	$log = \App\Logs::create([
		'added_on' => date('Y-m-d H:i:s', time()),
		'user_id' => Auth::id(),
		'description' => 'visited - view logs'
	]);
	
	$list = \App\Logs::select('l.*','u.name')->from('user_logs AS l')
				->join('users AS u','u.id','=','l.user_id')
				->limit(100)->orderBy('added_on', 'DESC')->get();
	
	return view('logs', ['logs' => $list]);
})->middleware('auth');

Route::get('/list', function(){
	$list = \App\Invoice::all();
	
	$log = \App\Logs::create([
		'added_on' => date('Y-m-d H:i:s', time()),
		'user_id' => Auth::id(),
		'description' => 'visited - view invoices list'
	]);
	
	return view('list', ['invoices' => $list]);
})->middleware('auth');

Route::get('/create', function(){
	$log = \App\Logs::create([
		'added_on' => date('Y-m-d H:i:s', time()),
		'user_id' => Auth::id(),
		'description' => 'visited - add invoice'
	]);
	
	return view('create');
})->middleware('auth');

Route::post('/create', 'HomeController@create');
