@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
					<a class="btn btn-primary" href="{{ url('/list') }}">View invoices</a>
					<a class="btn btn-primary" href="{{ url('/create') }}">Add invoice</a>
					@if(Auth::user()->is_admin)
						<a class="btn btn-primary" href="{{ url('/logs') }}">View logs</a>
					@endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
