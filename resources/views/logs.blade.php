@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Logs</div>

                <div class="panel-body">
					@foreach($logs as $key => $log)
						<p><b>{{ $key + 1}}: </b>
							{{ $log->added_on }} 
							{{ $log->name }}
							{{ $log->description }}
						</p>
					@endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
