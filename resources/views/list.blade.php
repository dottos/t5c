@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">List</div>

                <div class="panel-body">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Reference</th>
								<th>Amount</th>
								<th>Description</th>
								<th>Created at</th>
							</tr>
						</thead>
						<tbody>
							@foreach($invoices as $invoice)
							<tr>
								<td>{{ $invoice->reference }}</td>
								<td>{{ $invoice->amount }}</td>
								<td>{{ $invoice->description }}</td>
								<td>{{ date('m/d/Y', strtotime($invoice->created_at)) }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
